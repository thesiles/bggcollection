<?php

include_once __DIR__.'/vendor/autoload.php';

use Bgg\Application\Command\Item\CreateItemCommand;
use Bgg\Application\Command\Item\Handler\CreateItemHandler;
use Bgg\Infrastructure\CommandBus\SynchronousCommandBus;
use Bgg\Infrastructure\Persistence\ItemRepository;

$commandBus = new SynchronousCommandBus();

$itemRepository = new ItemRepository();
$commandHandler = new CreateItemHandler($itemRepository);

$commandBus->register(CreateItemCommand::class, $commandHandler);

$command = new CreateItemCommand(
    182028,
    'Through the Ages: A New Story of Civilization',
    'https://cf.geekdo-images.com/itemrep/img/32e-PrFMZ0-P_KsnZHApZazlPqc=/fit-in/246x300/pic2663291.jpg',
    3
);

$commandBus->execute($command);