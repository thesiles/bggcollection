<?php
/**
 * Created by PhpStorm.
 * User: thesiles
 * Project: BggCollection
 * Filename: ItemRepositoryInterface.php.
 */

namespace Bgg\Domain\Collection;

interface CollectionRepositoryInterface
{
    public function create(Collection $collection);
}
