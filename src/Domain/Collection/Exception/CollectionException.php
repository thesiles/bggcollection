<?php
/**
 * Created by PhpStorm.
 * User: thesiles
 * Project: BggCollection
 * Filename: ItemException.php.
 */

namespace Bgg\Domain\Collection\Exception;

class CollectionException extends \Exception
{
}
