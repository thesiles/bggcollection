<?php
/**
 * Created by PhpStorm.
 * User: thesiles
 * Project: BggCollection
 * Filename: ItemRepositoryInterface.php.
 */

namespace Bgg\Domain\Item;

interface ItemRepositoryInterface
{
    public function create(Item $item);
}
