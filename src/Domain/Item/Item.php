<?php
/**
 * Created by PhpStorm.
 * User: thesiles
 * Project: BggCollection
 * Filename: Item.php.
 */

namespace Bgg\Domain\Item;

class Item
{
    public $id;
    public $bggId;
    public $name;
    public $thumbnail;
    public $rank;
}
