<?php
/**
 * Created by PhpStorm.
 * User: thesiles
 * Project: BggCollection
 * Filename: ItemException.php.
 */

namespace Bgg\Domain\Item\Exception;

class ItemException extends \Exception
{
}
