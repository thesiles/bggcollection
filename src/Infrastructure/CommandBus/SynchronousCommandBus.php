<?php
/**
 * Created by PhpStorm.
 * User: thesiles
 * Project: BggCollection
 * Filename: SynchronousCommandBus.php.
 */

namespace Bgg\Infrastructure\CommandBus;

use Bgg\Application\Command\CommandBusInterface;
use Bgg\Application\Command\CommandHandlerInterface;
use Bgg\Application\Command\CommandInterface;

class SynchronousCommandBus implements CommandBusInterface
{
    /**
     * @var CommandHandlerInterface[]
     */
    private $handlers = [];

    /**
     * @param CommandInterface $command
     *
     * @throws \Exception
     *
     * @return mixed
     */
    public function execute(CommandInterface $command)
    {
        $commandName = get_class($command);

        /*
        // We'll need to check if the Command that's given
        // is actually registered to be handled here.
        */
        if (!array_key_exists($commandName, $this->handlers)) {
            throw new \Exception("{$commandName} is not supported by the SynchronousCommandBus");
        }

        return $this->handlers[$commandName]->handle($command);
    }

    /**
     * Now all we need is a function to register handlers
     * @param $commandName
     * @param CommandHandlerInterface $handler
     * @return $this
     */
    public function register($commandName, CommandHandlerInterface $handler)
    {
        $this->handlers[$commandName] = $handler;

        return $this;
    }
}
