<?php
/**
 * Created by PhpStorm.
 * User: thesiles
 * Project: BggCollection
 * Filename: PostRepository.php.
 */

namespace Bgg\Infrastructure\Persistence;

use Bgg\Domain\Item\Item;
use Bgg\Domain\Item\ItemRepositoryInterface;

class ItemRepository implements ItemRepositoryInterface
{
    public $items = [];

    public function create(Item $item)
    {
        $this->items[] = $item;

        // Obviously, this is for testing purposes only
        echo "Item with id {$item->id} was created." . PHP_EOL;
    }
}
