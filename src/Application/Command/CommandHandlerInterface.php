<?php
/**
 * Created by PhpStorm.
 * User: thesiles
 * Project: BggCollection
 * Filename: CommandHandlerInterface.php.
 */

namespace Bgg\Application\Command;

interface CommandHandlerInterface
{
    public function handle(CommandInterface $command);
}
