<?php
/**
 * Created by PhpStorm.
 * User: thesiles
 * Project: BggCollection
 * Filename: CommandBusInterface.php.
 */

namespace Bgg\Application\Command;

interface CommandBusInterface
{
    public function execute(CommandInterface $command);
}
