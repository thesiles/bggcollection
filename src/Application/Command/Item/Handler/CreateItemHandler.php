<?php
/**
 * Created by PhpStorm.
 * User: thesiles
 * Project: BggCollection
 * Filename: CreateItemHandler.php.
 */

namespace Bgg\Application\Command\Item\Handler;

use Bgg\Application\Command\CommandHandlerInterface;
use Bgg\Application\Command\CommandInterface;
use Bgg\Application\Command\Item\CreateItemCommand;
use Bgg\Domain\Item\Item;
use Bgg\Domain\Item\ItemRepositoryInterface;

class CreateItemHandler implements CommandHandlerInterface
{
    private $itemRepository;

    /*
    // We'll use constructor injection to inject this handlers
    // dependencies. We'll typehint the interface since we do not
    // care where to store it at this point.
    */

    /**
     * CreateItemHandler constructor.
     * @param ItemRepositoryInterface $itemRepository
     */
    public function __construct(ItemRepositoryInterface $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    /*
    // Since we are not permitted more specific types of the
    // CommandInterface, we'll have to check its type.
    */

    public function handle(CommandInterface $command)
    {
        if (!$command instanceof CreateItemCommand) {
            throw new \Exception("CreateItemHandler can only handle CreateItemCommand");
        }

        $item = new Item();
        $item->id = uniqid();
        $item->bggId = $command->getBggId();
        $item->name = $command->getName();
        $item->thumbnail = $command->getThumbnail();
        $item->rank = $command->getRank();


        $this->itemRepository->create($item);
    }
}
