<?php
/**
 * Created by PhpStorm.
 * User: thesiles
 * Project: BggCollection
 * Filename: CreateItemCommand.php.
 */

namespace Bgg\Application\Command\Item;

use Bgg\Application\Command\CommandInterface;

class CreateItemCommand implements CommandInterface
{
    private $bggId;
    private $name;
    private $thumbnail;
    private $rank;

    /**
     * CreateItemCommand constructor.
     *
     * @param $bggId
     * @param $name
     * @param $thumbnail
     * @param $rank
     */
    public function __construct(
        $bggId,
        $name,
        $thumbnail,
        $rank
    ) {
        $this->bggId = $bggId;
        $this->name = $name;
        $this->thumbnail = $thumbnail;
        $this->rank = $rank;
    }

    /**
     * @return int
     */
    public function getBggId()
    {
        return $this->bggId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * @return int
     */
    public function getRank()
    {
        return $this->rank;
    }
}
